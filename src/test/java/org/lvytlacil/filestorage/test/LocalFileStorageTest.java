package org.lvytlacil.filestorage.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;


import org.junit.Before;
import org.junit.Test;
import org.lvytlacil.filestorage.filestorage.LocalFileStorage;
import org.lvytlacil.filestorage.filestorage.StoredRecord;
import org.lvytlacil.filestorage.request.FileUploadRequest;

public class LocalFileStorageTest {

    private ResourceProviderMock resProvider;
    private LocalFileStorage storage;

    @Before
    public void init() {
        storage = new LocalFileStorage();
        resProvider = new ResourceProviderMock();
        storage.setPathProvider(resProvider);
    }

    @Test
    public void getFilesReturnNonEmptyListWithCorrectlyFormedRecords() {
        List<StoredRecord> records = storage.getFiles();
        assertNotNull("Listing records should return non null list", records);
        assertTrue("Listing records should return non empty list", records.size() > 0);

        StoredRecord record = records.get(0);
        assertNotNull("Sample record't title should not be null", record.getTitle());
    }

    @Test
    public void uploadingFileCreatesFileInTheStorage() {
        // preparation
        String path = createDummyFileForUpload();
        InputStream is;
        try {
            is = new FileInputStream(path);
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Error accessing dummy file");
        }
        FileUploadRequest request = new FileUploadRequest("soubor", "Toto je muj soubor", "11-11-2011",
                "sampleFile.txt", 1452, is);
        // test
        File storageFolder = new File(resProvider.getStorageFolderPath());
        long fileCount = storageFolder.list().length;
        
        storage.uploadFile(request);
        
        long updatedFileCount = storageFolder.list().length;
        assertEquals("Uploaded file should have increased the number of files in the sotrage by 1.", 
                fileCount + 1, updatedFileCount);

    }
    
    @Test
    public void uploadingFileCreatesEntryInTheDatbase() {
        // preparation
        String path = createDummyFileForUpload();
        InputStream is;
        try {
            is = new FileInputStream(path);
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Error accessing dummy file");
        }
        FileUploadRequest request = new FileUploadRequest("soubor", "Toto je muj soubor", "11-11-2011",
                "sampleFile.txt", 1452, is);
        // test
        long recordCount = storage.getFiles().size();
        
        storage.uploadFile(request);
        
        long updatedRecordCount = storage.getFiles().size();
        assertEquals("Uploaded file should have increased the number of records in the databsae by 1.", 
                recordCount + 1, updatedRecordCount);

    }

    private String createDummyFileForUpload() {
        final String path = "target/sampleFile.txt";
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(path))) {
            bw.write("Something to write.");
        } catch (IOException e) {
            throw new RuntimeException("Error creating dummy file");
        }
        return path;

    }
}
