package org.lvytlacil.filestorage.filestorage;

/**
 * Represents info about a single file that is meant to be presented to clients listing through
 * the storage.
 * 
 * @author lvytlacil
 *
 */
public class StoredRecord {

	private long id;
	private String description;
	private String uploadTime;
	private String title;
	private String origname;
	private long size;
	
	public StoredRecord() { };

	public StoredRecord(long id, String description, String uploadTime, String title, String filename, long size) {
		this.id = id;
		this.description = description;
		this.uploadTime = uploadTime;
		this.title = title;
		this.origname = filename;
		this.size = size;
	}

	public long getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}

	public String getUploadTime() {
		return uploadTime;
	}

	public String getTitle() {
		return title;
	}

	public String getOrigname() {
		return origname;
	}

	public long getSize() {
		return size;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setUploadTime(String uploadTime) {
		this.uploadTime = uploadTime;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setFilename(String filename) {
		this.origname = filename;
	}

	public void setSize(long size) {
		this.size = size;
	}
	
	

}
