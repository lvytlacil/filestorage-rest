package org.lvytlacil.filestorage.request;

/**
 * Encapsulates a request for removing a file.
 * 
 * @author lvytlacil
 *
 */
public class FileRemoveRequest {
	private String id;

	/**
	 * Creates a new instance encapsulating a request for deleting a file with the given id.
	 * 
	 * @param id
	 */
	public FileRemoveRequest(String id) {
		this.id = id;
	}

	/**
	 * 
	 * @return requested id
	 */
	public String getId() {
		return id;
	}

}
