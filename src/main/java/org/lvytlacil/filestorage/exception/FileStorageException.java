package org.lvytlacil.filestorage.exception;

public class FileStorageException extends RuntimeException {

	private static final long serialVersionUID = -6792761905359877132L;

	private HttpError httpError;
	
	public FileStorageException(ServiceError error) {
		this.httpError = new HttpError(400, error);
	}
	
	public FileStorageException(HttpError error) {
		this.httpError = error;
	}
	
	public FileStorageException(ServiceError error, Throwable cause) {
		super(cause);
		this.httpError = new HttpError(400, error);
	}

	public HttpError getHttpError() {
		return httpError;
	}

	
}
