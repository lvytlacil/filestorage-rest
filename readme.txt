Implementation of simple file storage service/
This simple implementation uses a folder in the user's home directory for storing the uploaded files and keeps
track of them via an SQLite database.

Tests use a test storage/database that does not interfere with the real storage/database.

REST API:

/rest/files/list
A GET request that retrieves list of records of all uploaded files and presents it to the caller as a JSON String.
Each record contains relevant information about a single uploaded file.

/rest/files/download/{id}
A GET request that initiates downloading of a single uploaded file identified by its Id. Default name
for the downloaded file is chosen to be the original name of the uploaded file.

/rest/files/delete/{id}
A GET request that deletes a single uploaded file identified by its Id, if it is present, and returns a JSON String
with information about the deleted file.

/rest/files/upload
A POST request that uploads a file to the storage.
This expect body with the following form data:
  title (text): Title to be used for the uploaded file.
  description (text): Description of the uploaded file.
  file (file): The file to upload
  
How to Deploy:
  The source code in the repository contains maven configuration. You can clone it
  and run "mvn package", which creates a .war file in the target folder that can be deployed to a Tomcat server for example.