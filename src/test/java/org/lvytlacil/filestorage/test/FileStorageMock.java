package org.lvytlacil.filestorage.test;

import java.util.ArrayList;
import java.util.List;

import org.lvytlacil.filestorage.filestorage.FileStorage;
import org.lvytlacil.filestorage.filestorage.StoredRecord;
import org.lvytlacil.filestorage.request.FileDownloadRequest;
import org.lvytlacil.filestorage.request.FileRemoveRequest;
import org.lvytlacil.filestorage.request.FileUploadRequest;

/**
 * Mock of {@link FileStorage} to be used for testing.
 * 
 * @author lvytlacil
 *
 */
public class FileStorageMock implements FileStorage {

	@Override
	public StoredRecord uploadFile(FileUploadRequest uploadRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<StoredRecord> getFiles() {
		List<StoredRecord> result = new ArrayList<StoredRecord>();
		StoredRecord record = new StoredRecord(1, "Vyuka lolcodu", "11-11-2011", "lolko", "lolcode.pdf", 4540474);
		result.add(record);
		record = new StoredRecord(2, "Collection of poems", "12-12-2014", "poems", "poems.txt", 11224);
		result.add(record);
		return result;
	}

	@Override
	public LocatedFile locateFile(FileDownloadRequest downloadRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public StoredRecord deleteFile(FileRemoveRequest removeRequest) {
		// TODO Auto-generated method stub
		return null;
	}

}
