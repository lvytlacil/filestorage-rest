package org.lvytlacil.filestorage.exception;

public class HttpError {
	private int statusCode;
	private ServiceError serviceError;

	public HttpError(int statusCode, ServiceError serviceError) {
		this.statusCode = statusCode;
		this.serviceError = serviceError;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public ServiceError getServiceError() {
		return serviceError;
	}

}
