package org.lvytlacil.filestorage.test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.lvytlacil.filestorage.filestorage.resourceprovider.ResourceProvider;

/**
 * Implementation of {@link ResourceProvider} that uses a test database.
 * 
 * @author lvytlacil
 *
 */
public class ResourceProviderMock implements ResourceProvider {

    private static final String STORAGE_FOLDER_RELATIVE_PATH = "target/testdata/storage";

    private static final String DB_RELATIVE_PATH = "target/testdata/catalog.db";

    static {
        // register driver
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e1) {
            e1.printStackTrace();
        }
    }
    
    /**
     * Creates an instance providing access to storage and
     * file record database, both of which are consistently cleaned and
     * then filled with default values.
     * Upon creation, there is at least one uploaded in the storage.
     */
    public ResourceProviderMock() {
        reset();
    }

    /**
     * Consistently wipes the storage and database and fills
     * them with default values. After completion, there will
     * be at least one uploaded file in the storage.
     */
    public void reset() {

        File dbFile = new File(DB_RELATIVE_PATH);
        if (dbFile.exists()) {
            dbFile.delete();
        }

        File storageFolder = new File(STORAGE_FOLDER_RELATIVE_PATH);
        try {
            FileUtils.deleteDirectory(storageFolder);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // recreate folders
        storageFolder.mkdirs();

        try (Connection connection = p_getDbConnection(); Statement stmt = connection.createStatement();) {
            stmt.executeUpdate("CREATE TABLE filerecord " + "(id INT PRIMARY KEY, "
                    + "title TEXT, description TEXT, uploadtime TEXT, origname TEXT, size INT, path TEXT)");

            insertRecord(stmt, 1, "borovice", "borovice na zahrade", "11-11-2012", "3213.png", 14254, "strom.png");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private static void insertRecord(Statement stmt, long id, String title, String description, String uploadTime,
            String origName, long size, String newName) throws SQLException {

        String query = String.format(
                "INSERT INTO filerecord (id, title, description, uploadtime, origname, size, path) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s')",
                id, title, description, uploadTime, origName, size, createDummyFile(newName));
        stmt.executeUpdate(query);

    }

    private static String createDummyFile(String fileName) {
        String filePath = Paths.get(STORAGE_FOLDER_RELATIVE_PATH, fileName).toString();
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(filePath))) {
            bw.append("Hello, this is a test file");
            bw.append(UUID.randomUUID().toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return filePath;
    }

    private static Connection p_getDbConnection() {
        Connection connection = null;
        try {

            connection = DriverManager.getConnection("jdbc:sqlite:" + DB_RELATIVE_PATH);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    @Override
    public String getStorageFolderPath() {
        return STORAGE_FOLDER_RELATIVE_PATH;
    }

    @Override
    public Connection getDbConnection() {
        return p_getDbConnection();
    }

}
