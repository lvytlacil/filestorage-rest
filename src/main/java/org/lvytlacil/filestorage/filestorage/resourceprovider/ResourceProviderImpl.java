package org.lvytlacil.filestorage.filestorage.resourceprovider;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Implementation of {@link ResourceProvider} that uses a SQLite database for storing
 * file records and a local folder in the user's home directory for storing uploaded files.
 * 
 * @author lvytlacil
 *
 */
public class ResourceProviderImpl implements ResourceProvider {

	/** Path to the storage root folder */
	private static final String STORAGE_FOLDER_PATH = System.getProperty("user.home") + "/lvytlacil_homework/storage";

	/** Path to the database */
	private static final String DB_PATH = System.getProperty("user.home") + "/lvytlacil_homework/catalog.db";
	
	// Code for preparing storage and database
	static {
		
		File storageFolder = new File(STORAGE_FOLDER_PATH);
		if (!storageFolder.exists()) {
			storageFolder.mkdirs();
		}
		
		try {
			Class.forName("org.sqlite.JDBC");
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		File dbFolder = new File(DB_PATH).getParentFile();
		if (!dbFolder.exists()) {
			dbFolder.mkdirs();
		}
		
		Connection connection = p_getDbConnection();
		if (connection != null) {
			try {
				Statement stmt = connection.createStatement();
				stmt.executeUpdate("CREATE TABLE IF NOT EXISTS filerecord " + "(id INT PRIMARY KEY, "
				        + "title TEXT, description TEXT, uploadtime TEXT, origname TEXT, size INT, path TEXT)");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	// Retrieves connection
	private static Connection p_getDbConnection() {
		Connection connection = null;
		try {
			connection = DriverManager.getConnection("jdbc:sqlite:" + DB_PATH);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return connection;
	}

	@Override
	public String getStorageFolderPath() {
		return STORAGE_FOLDER_PATH;
	}

	@Override
	public Connection getDbConnection() {
		return p_getDbConnection();
	}

}
