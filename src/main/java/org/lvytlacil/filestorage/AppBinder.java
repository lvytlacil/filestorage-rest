package org.lvytlacil.filestorage;

import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.lvytlacil.filestorage.filestorage.FileStorage;
import org.lvytlacil.filestorage.filestorage.LocalFileStorage;
import org.lvytlacil.filestorage.filestorage.resourceprovider.ResourceProvider;
import org.lvytlacil.filestorage.filestorage.resourceprovider.ResourceProviderImpl;

public class AppBinder extends AbstractBinder {

	@Override
	protected void configure() {
		bind(LocalFileStorage.class).to(FileStorage.class);
		bind(ResourceProviderImpl.class).to(ResourceProvider.class);
	}

}
