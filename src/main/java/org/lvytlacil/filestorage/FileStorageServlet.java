package org.lvytlacil.filestorage;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.lvytlacil.filestorage.exception.FileStorageException;
import org.lvytlacil.filestorage.exception.ServiceError;
import org.lvytlacil.filestorage.filestorage.FileStorage;
import org.lvytlacil.filestorage.filestorage.FileStorage.LocatedFile;
import org.lvytlacil.filestorage.request.FileDownloadRequest;
import org.lvytlacil.filestorage.request.FileRemoveRequest;
import org.lvytlacil.filestorage.request.FileUploadRequest;
import org.lvytlacil.filestorage.filestorage.StoredRecord;

@Path("/storage")
public class FileStorageServlet {

	private final static Logger LOGGER = Logger.getLogger(FileStorageServlet.class.getName());

	@Inject
	private FileStorage fileStorage;

	/**
	 * 
	 * @return List of stored files. Each entry contains relevant information about one particular
	 * file that is meant to be accessible to the client.
	 */
	@GET
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	public List<StoredRecord> getFiles() {
		return fileStorage.getFiles();
	}

	@POST
	@Path("/upload")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public StoredRecord uploadFile(@FormDataParam("title") String title,
	        @FormDataParam("description") String description, @FormDataParam("file") InputStream stream,
	        @FormDataParam("file") FormDataContentDisposition fileDetail) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		String dateAsString = formatter.format(new Date());

		System.out.println(fileDetail.getSize());

		FileUploadRequest uploadRequest = new FileUploadRequest(title, description, dateAsString,
		        fileDetail.getFileName(), fileDetail.getSize(), stream);
		LOGGER.log(Level.INFO, "Created upload request");
		LOGGER.log(Level.INFO, String.format("title: %s", uploadRequest.getTitle()));
		LOGGER.log(Level.INFO, String.format("description: %s", uploadRequest.getDescription()));
		LOGGER.log(Level.INFO, String.format("uploadtime: %s", uploadRequest.getUploadTime()));
		LOGGER.log(Level.INFO, String.format("origname: %s", uploadRequest.getFilename()));
		LOGGER.log(Level.INFO, String.format("size: %s", uploadRequest.getSize()));
		LOGGER.log(Level.INFO, String.format("size: %s", uploadRequest.getInputStream()));

		return fileStorage.uploadFile(uploadRequest);
	}

	@GET
	@Path("download/{id}")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response downloadFile(@PathParam("id") String id) {
		LocatedFile locatedFile = fileStorage.locateFile(new FileDownloadRequest(id));

		if (Files.notExists(locatedFile.getPath())) {
			throw new FileStorageException(
			        new ServiceError(String.format("Could not locate requested file with id=%s", id)));
		}

		Response result = Response.ok(new StreamingOutput() {

			@Override
			public void write(OutputStream output) throws IOException, WebApplicationException {
				Files.copy(locatedFile.getPath(), output);
			}
		}).header("Content-Disposition", String.format("attachment; filename=\"%s\"", locatedFile.getOriginalName()))
		        .build();
		return result;
	}
	
	@GET
	@Path("delete/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public StoredRecord removeFile(@PathParam("id") String id) {
		return fileStorage.deleteFile(new FileRemoveRequest(id));
	}
}
