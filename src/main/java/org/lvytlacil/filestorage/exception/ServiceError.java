package org.lvytlacil.filestorage.exception;

public class ServiceError {
	private String message;

	public ServiceError(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

}
