package org.lvytlacil.filestorage.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class ApplicationExceptionMapper implements ExceptionMapper<FileStorageException> {

	@Override
	public Response toResponse(FileStorageException exception) {

		return Response.status(exception.getHttpError().getStatusCode())
		        .entity(exception.getHttpError().getServiceError()).build();
	}

}
