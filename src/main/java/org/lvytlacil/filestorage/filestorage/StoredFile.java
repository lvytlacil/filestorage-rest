package org.lvytlacil.filestorage.filestorage;

/**
 * Represents a single uploaded file.
 * 
 * @author lvytlacil
 *
 */
public class StoredFile {

	private StoredRecord storedRecord;

	private String path;

	public StoredFile(StoredRecord storedRecord, String path) {
		this.storedRecord = storedRecord;
		this.path = path;
	}

	/**
	 * 
	 * @return All information that is meant to be displayed to clients listing through the file storage.
	 */
	public StoredRecord getStoredRecord() {
		return storedRecord;
	}

	/**
	 * 
	 * @return Full path to the actual file in the storage.
	 */
	public String getPath() {
		return path;
	}

}
