package org.lvytlacil.filestorage.request;

import java.io.InputStream;

/**
 * Represents a request for a file upload.
 * 
 * @author lvytlacil
 *
 */
public class FileUploadRequest {

	private String title;
	private String description;
	private String uploadTime;

	private String filename;
	private long size;
	private InputStream inputStream;

	/**
	 * Creates an encapsulation of a file upload request.
	 * 
	 * @param title title (or display name) to associate with the uploaded file
	 * @param description description of the uploaded file
	 * @param uploadTime string representation of the time an upload was initiated
	 * @param filename original name of the uploaded file
	 * @param size size in bytes of the uploaded file
	 * @param inputStream stream through which the actual uploaded data can be accessed
	 */
	public FileUploadRequest(String title, String description, String uploadTime, String filename, long size,
	        InputStream inputStream) {
		this.title = title;
		this.description = description;
		this.uploadTime = uploadTime;
		this.filename = filename;
		this.size = size;
		this.inputStream = inputStream;
	}

	/**
	 * 
	 * @return title (or display name)
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * 
	 * @return description of the uploaded file
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 
	 * @return string representation of the time the upload was initiated
	 */
	public String getUploadTime() {
		return uploadTime;
	}

	/**
	 * 
	 * @return original file name
	 */
	public String getFilename() {
		return filename;
	}

	/**
	 * 
	 * @return size of the uploaded file in bytes
	 */
	public long getSize() {
		return size;
	}

	/**
	 * 
	 * @return stream through which the actual uploaded data can be accessed
	 */
	public InputStream getInputStream() {
		return inputStream;
	}

}
