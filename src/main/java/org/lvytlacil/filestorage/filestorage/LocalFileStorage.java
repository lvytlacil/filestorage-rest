package org.lvytlacil.filestorage.filestorage;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import org.apache.commons.io.FileUtils;
import org.lvytlacil.filestorage.exception.FileStorageException;
import org.lvytlacil.filestorage.exception.ServiceError;
import org.lvytlacil.filestorage.filestorage.resourceprovider.ResourceProvider;
import org.lvytlacil.filestorage.request.FileDownloadRequest;
import org.lvytlacil.filestorage.request.FileRemoveRequest;
import org.lvytlacil.filestorage.request.FileUploadRequest;

public class LocalFileStorage implements FileStorage {

	@Inject
	private ResourceProvider resourceProvider;
	
	public void setPathProvider(ResourceProvider pathProvider) {
        this.resourceProvider = pathProvider;
    }

    @Override
	public StoredRecord uploadFile(FileUploadRequest uploadRequest) {
		if (uploadRequest == null) {
			throw new FileStorageException(new ServiceError("Missing upload data"));
		}

		// Upload the file first
		String uploadedFilePath = writeFile(uploadRequest);

		// If the sender did not include size
		long fileSize = uploadRequest.getSize() < 0 ? (FileUtils.sizeOf(new File(uploadedFilePath)))
		        : uploadRequest.getSize();

		// Store record entry into the database
		StoredRecord result = storeRecordToDb(uploadRequest, uploadedFilePath, fileSize);

		if (result != null) {
			return result;
		} else {

			// Record not stored into the db, rollback and remove the uploaded file now
			try {
				Files.deleteIfExists(Paths.get(uploadedFilePath));
			} catch (IOException e) {
				throw new FileStorageException(new ServiceError(
				        "Could not store record into the database and could not rollback (delete already uploaded file)"),
				        e);
			}
			throw new FileStorageException(new ServiceError(
			        "Could not upload the file because not being able to store record into the database"));
		}
	}

	// Store a single file record into the database. If this fails, it returns null
	// so that
	// a rollback can be executed --- removing the actual file that may have already
	// been stored
	private StoredRecord storeRecordToDb(FileUploadRequest uploadRequest, String uploadedFilePath, long fileSize) {

		try (Connection connection = resourceProvider.getDbConnection();
		        PreparedStatement insertStmt = connection.prepareStatement(
		                "INSERT INTO filerecord (id, title, description, uploadtime, origname, size, path) "
		                        + "VALUES (?, ?, ?, ?, ?, ?, ?)");

		        Statement queryStmt = connection.createStatement();) {

			ResultSet maxIdResultSet = queryStmt.executeQuery("SELECT MAX(id) from filerecord");

			long newId = maxIdResultSet.next() ? maxIdResultSet.getLong(1) + 1 : 0;

			insertStmt.setLong(1, newId);
			insertStmt.setString(2, uploadRequest.getTitle());
			insertStmt.setString(3, uploadRequest.getDescription());
			insertStmt.setString(4, uploadRequest.getUploadTime());
			insertStmt.setString(5, uploadRequest.getFilename());
			insertStmt.setLong(6, fileSize);
			insertStmt.setString(7, uploadedFilePath);

			insertStmt.executeUpdate();

			return new StoredRecord(newId, uploadRequest.getDescription(), uploadRequest.getUploadTime(),
			        uploadRequest.getTitle(), uploadRequest.getFilename(), fileSize);
		} catch (SQLException e) {
			return null;
		}

	}

	private String writeFile(FileUploadRequest uploadRequest) {
		String targetFileName = UUID.randomUUID().toString();
		Path targetFilePath = Paths.get(resourceProvider.getStorageFolderPath(), targetFileName);
		try {
			Files.copy(uploadRequest.getInputStream(), targetFilePath);
		} catch (IOException e) {
			throw new FileStorageException(new ServiceError("Failed to upload the requested file."), e);
		}
		return targetFilePath.toString();
	}

	// Retrieving list of records

	@Override
	public List<StoredRecord> getFiles() {
		Connection connection = resourceProvider.getDbConnection();

		List<StoredRecord> retrievedRecords = new ArrayList<StoredRecord>();
		try {
			Statement stmt = connection.createStatement();
			ResultSet result = stmt
			        .executeQuery("SELECT id, title, description, uploadtime, origname, size FROM filerecord");
			while (result.next()) {
				StoredRecord record = new StoredRecord();
				record.setId(result.getInt(1));
				record.setTitle(result.getString(2));
				record.setDescription(result.getString(3));
				record.setUploadTime(result.getString(4));
				record.setFilename(result.getString(5));
				record.setSize(result.getLong(6));
				retrievedRecords.add(record);
			}
		} catch (SQLException e) {
			throw new FileStorageException(new ServiceError("Could not retrieve the list of stored files"), e);
		}

		return retrievedRecords;
	}

	@Override
	public LocatedFile locateFile(FileDownloadRequest downloadRequest) {

		if (downloadRequest == null) {
			throw new FileStorageException(new ServiceError("Missing download request"));
		}

		int requestedId = -1;
		try {
			requestedId = Integer.valueOf(downloadRequest.getId());
		} catch (NumberFormatException ex) {
			throw new FileStorageException(new ServiceError("Requested Id was not a number"), ex);
		}

		return locateFileWithId(requestedId);
	}

	private LocatedFile locateFileWithId(int requestedId) {
		try (Connection connection = resourceProvider.getDbConnection();
		        PreparedStatement stmt = connection
		                .prepareStatement("SELECT origname, path FROM filerecord WHERE id = ?");) {
			stmt.setLong(1, requestedId);
			ResultSet resultSet = stmt.executeQuery();

			if (resultSet.next()) {
				return new LocatedFile(Paths.get(resultSet.getString(2)), resultSet.getString(1));
			} else {
				throw new FileStorageException(new ServiceError("Could not locate file with the given Id"));
			}

		} catch (SQLException e) {
			throw new FileStorageException(new ServiceError("Could not store record into the database"));
		}
	}

	@Override
	public StoredRecord deleteFile(FileRemoveRequest removeRequest) {
		if (removeRequest == null) {
			throw new FileStorageException(new ServiceError("Missing remove request"));
		}

		int requestedId = -1;
		try {
			requestedId = Integer.valueOf(removeRequest.getId());
		} catch (NumberFormatException ex) {
			throw new FileStorageException(new ServiceError("Requested Id was not a number"), ex);
		}

		StoredFile result = getStoredRecordById(requestedId);
		removeFileRecordById(requestedId);

		try {
			Files.delete(Paths.get(result.getPath()));
		} catch (IOException e) {
			throw new FileStorageException(new ServiceError(
			        "Record removed from database, but the actual file could not be removed from storage."), e);
		}

		return result.getStoredRecord();

	}

	private void removeFileRecordById(long id) {
		try (Connection connection = resourceProvider.getDbConnection();
		        PreparedStatement stmt = connection.prepareStatement("DELETE FROM filerecord WHERE id = ?");) {
			stmt.setLong(1, id);
			stmt.execute();
		} catch (SQLException e) {
			throw new FileStorageException(new ServiceError("Unable to remove a record with the given id"), e);
		}
	}

	private StoredFile getStoredRecordById(long id) {
		try (Connection connection = resourceProvider.getDbConnection();
		        PreparedStatement stmt = connection.prepareStatement(
		                "SELECT id, title, description, uploadtime, origname, size, path FROM filerecord WHERE id = ?");) {
			stmt.setLong(1, id);
			ResultSet result = stmt.executeQuery();

			if (result.next()) {
				StoredRecord record = new StoredRecord();
				record.setId(result.getInt(1));
				record.setTitle(result.getString(2));
				record.setDescription(result.getString(3));
				record.setUploadTime(result.getString(4));
				record.setFilename(result.getString(5));
				record.setSize(result.getLong(6));

				StoredFile storedFile = new StoredFile(record, result.getString(7));
				return storedFile;
			} else {
				throw new FileStorageException(
				        new ServiceError("There was no record with the given Id in the database"));
			}
		} catch (SQLException e) {
			throw new FileStorageException(
			        new ServiceError("Could not retrieve a record with the given Id from the database"), e);
		}
	}

}
