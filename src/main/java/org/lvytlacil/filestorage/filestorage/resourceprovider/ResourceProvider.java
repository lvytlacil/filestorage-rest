package org.lvytlacil.filestorage.filestorage.resourceprovider;

import java.sql.Connection;

/**
 * Provides resources for a simple local file storage.
 * 
 * @author lvytlacil
 *
 */
public interface ResourceProvider {
	/**
	 * 
	 * @return String representation of the root folder of the storage.
	 */
	String getStorageFolderPath();
	
	/**
	 * Returns a connection to the database with file records. Clients are required to
	 * close the received connection properly.
	 * 
	 * @return Connection Connection to the database with file records.
	 */
	Connection getDbConnection();
}
