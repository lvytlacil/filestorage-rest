package org.lvytlacil.filestorage.request;

/**
 * Encapsulates a request for a file download.
 * 
 * @author lvytlacil
 *
 */
public class FileDownloadRequest {
	String requestedId;

	/**
	 * Creates a new instance encapsulating a request for a file of the given id.
	 * @param id requested id
	 */
	public FileDownloadRequest(String id) {
		this.requestedId = id;
	}

	/**
	 * 
	 * @return requested id
	 */
	public String getId() {
		return requestedId;
	}
}
