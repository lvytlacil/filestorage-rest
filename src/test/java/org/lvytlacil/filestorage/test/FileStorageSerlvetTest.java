package org.lvytlacil.filestorage.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;
import org.lvytlacil.filestorage.FileStorageServlet;
import org.lvytlacil.filestorage.exception.ApplicationExceptionMapper;
import org.lvytlacil.filestorage.filestorage.FileStorage;
import org.lvytlacil.filestorage.filestorage.StoredRecord;

import com.fasterxml.jackson.databind.ObjectMapper;

public class FileStorageSerlvetTest extends JerseyTest {

	@Override
	protected Application configure() {
		AbstractBinder binder = new AbstractBinder() {
			@Override
			protected void configure() {
				bind(FileStorageMock.class).to(FileStorage.class);
			}
		};
		ResourceConfig config = new ResourceConfig(FileStorageServlet.class);
		config.register(binder);
		config.register(MultiPartFeature.class);
		config.register(ApplicationExceptionMapper.class);
		return config;
	}

	@Test
	public void getAllFileRecordsTest() {
		Response response = target("/storage/list").request(MediaType.APPLICATION_JSON).get(Response.class);

		assertEquals("Response should be 200 (OK)", Status.OK.getStatusCode(), response.getStatus());
		
		ObjectMapper mapper = new ObjectMapper();
		String records = response.readEntity(String.class);
		
		assertNotNull("Response content should not be null", records);
		try {
			List<StoredRecord> parsed = Arrays.asList(mapper.readValue(records, StoredRecord[].class));
			System.out.println(parsed.size());
			assertNotNull("Parsed collection should not be null", parsed);
			assertTrue("There should be some records in the collection.", parsed.size() > 0);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
