package org.lvytlacil.filestorage.filestorage;

import java.nio.file.Path;
import java.util.List;

import org.lvytlacil.filestorage.exception.FileStorageException;
import org.lvytlacil.filestorage.request.FileDownloadRequest;
import org.lvytlacil.filestorage.request.FileRemoveRequest;
import org.lvytlacil.filestorage.request.FileUploadRequest;

public interface FileStorage {
	StoredRecord uploadFile(FileUploadRequest uploadRequest) throws FileStorageException;

	List<StoredRecord> getFiles();

	LocatedFile locateFile(FileDownloadRequest downloadRequest);
	
	StoredRecord deleteFile(FileRemoveRequest removeRequest);

	static class LocatedFile {
		private final Path path;
		private final String originalName;

		public LocatedFile(Path path, String originalName) {
			this.path = path;
			this.originalName = originalName;
		}

		public Path getPath() {
			return path;
		}

		public String getOriginalName() {
			return originalName;
		}

	}
}
