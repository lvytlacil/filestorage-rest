package org.lvytlacil.filestorage;

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.lvytlacil.filestorage.exception.ApplicationExceptionMapper;

public class Application extends ResourceConfig {
	public Application() {
		
		register(new AppBinder());
		register(MultiPartFeature.class);
		register(ApplicationExceptionMapper.class);
		packages(true, "org.lvytlacil.filestorage");
	}
}
